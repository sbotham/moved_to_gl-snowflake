#!/usr/bin/env python

import snowflake.connector
import random
import os

# ARC

import Subscription
import Utils

Utils.clear()

TABLE="SUBSCRIPTIONS"

# Gets the version
ctx = snowflake.connector.connect(
    user=os.environ['TIP_SF_USER'],
    password=os.environ['TIP_SF_PASSWORD'],
    account=os.environ['TIP_SF_ACCOUNT'],
    authenticator=os.environ['TIP_SF_AUTHENTICATOR'],
    database=os.environ['TIP_SF_DB'],
    schema=os.environ['TIP_SF_SCHEMA'],
    role=os.environ['TIP_SF_ROLE'],
    warehouse=os.environ['TIP_SF_WAREHOUSE'],
)

def getVersion():
   cs = ctx.cursor()
   try:
     cs.execute("SELECT current_version()")
     one_row = cs.fetchone()

     print("get Version result="+one_row[0])
   finally:
     cs.close()


def getCount():
   cur = ctx.cursor()
   try:
      cur.execute("SELECT count(*) from "+TABLE)
      print('getCount result')
      for (col1) in cur:
         print("Count="+'{0}'.format(col1))
   finally:
     cur.close()
   return

def getAll(): 

# Example where
#   WHERE=" WHERE customer.c_name='Customer#000000003' and customer.C_phone='11-719-748-3364'"
   WHERE=""
   LIMIT=" LIMIT 5"
   LIMIT=""
   cur = ctx.cursor()
   try:
     SELECT="SELECT * FROM "+TABLE
     FULL=SELECT+WHERE+LIMIT
     cur.execute(FULL)
     print('getAll result')
     for (col1) in cur:
        print("Customers="+'{0}'.format(col1))
   finally:
     cur.close()
   return

def insert():
   cur = ctx.cursor()
   try:
     ACCT_ID=str(random.randint(1,101))
     ACCT_NAME="Name for ACCT_ID "+ACCT_ID
     INSERT="INSERT INTO "+TABLE+" ( ACCT_ID, ACCT_NAME ) VALUES ('"+ACCT_ID+"', '"+ACCT_NAME+"')"


     FULL=INSERT
     print("Insert="+INSERT)
     cur.execute(FULL)
     print('Insert result')
     for (col1) in cur:
        print('{0}'.format(col1))
   finally:
     cur.close()
   return

def delete():
   cur = ctx.cursor()
   WHERE=" WHERE ACCT_ID='6789'"
   try:
     DELETE="DELETE FROM "+TABLE+WHERE
     FULL=DELETE
     cur.execute(FULL)
     for (col1) in cur:
        print('Delete result='+'{0}'.format(col1))
   finally:
     cur.close()
   return

def create ():
   cur = ctx.cursor()
   CREATE="CREATE TABLE "+TABLE+" (ACCT_ID VARCHAR(20), ACCT_NAME VARCHAR(40),"
   PRIMARY=" PRIMARY KEY (ACCT_ID))"
   print(CREATE)
   try:
     FULL=CREATE+PRIMARY
     cur.execute(FULL)
     for (col1) in cur:
        print('Create result='+'{0}'.format(col1))
   finally:
     cur.close()
   return

def alter():
   cur = ctx.cursor()
   ALTER="ALTER TABLE "+TABLE+" ADD COLUMN ACCT_NAME VARCHAR(40)"
   try:
     FULL=ALTER
     cur.execute(FULL)
     for (col1) in cur:
        print('Alter result='+'{0}'.format(col1))
   finally:
     cur.close()
   return

def update():
   WHERE=" WHERE ACCT_ID='41'"
   SET=" SET ACCT_NAME='XYZ'"
   cur = ctx.cursor()
   try:
     UPDATE="UPDATE "+TABLE+ SET + WHERE
     print(UPDATE)
     FULL=UPDATE+WHERE
     cur.execute(FULL)
     for (col1) in cur:
        print('Update result='+'{0}'.format(col1))
   finally:
     cur.close()
   return

def drop():
   cur = ctx.cursor()
   DROP="DROP TABLE "+TABLE
   try:
     FULL=DROP 
     cur.execute(FULL)
     for (col1) in cur:
        print('Drop result='+'{0}'.format(col1))
   finally:
     cur.close()
   return


#sub = Subscription.Subscription("1", "2");
#print(sub)

#getVersion();
getCount();
#insert();
#delete();
#update();
getAll();
#alter();
#drop();
#create();

ctx.close();
