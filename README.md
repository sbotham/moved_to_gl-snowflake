
Simple python module to do various operations to a table in snowflake

please change the table name from SUBSCRIPTIONS before running i
any of the functions so you dont mess up my data :-)

pre requisites
install python3
https://www.python.org/downloads/

install python connector for snowflake
https://docs.snowflake.net/manuals/user-guide/python-connector-install.html

setup required env vars
see...

https://arccorp.atlassian.net/wiki/spaces/TT/pages/edit/385614409?draftId=385516577&draftShareId=446e4f38-1bfe-4f1e-8788-e7a6c0dc45e9&

edit the block at the bottom of validate.py to uncomment out the 
functions you want to test and then 
run with

python3 validate.py




